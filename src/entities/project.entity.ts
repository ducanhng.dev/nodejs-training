import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { Ticket } from './ticket.entity';

@Entity({
  name: 'projects',
})
export class Project {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    type: 'varchar',
    length: 255,
    name: 'name',
  })
  name: string;

  @Column({
    name: 'started_at',
    type: 'timestamp',
  })
  startedAt?: Date;

  @Column({
    name: 'ended_at',
    type: 'timestamp',
  })
  endedAt?: Date;

  @Column({
    name: 'classify',
    type: 'varchar',
    length: 255,
  })
  classify: string;

  @Column({
    name: 'profit',
    type: 'float',
  })
  profit?: number;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updatedAt?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true,
  })
  deletedAt?: Date;

  @OneToMany(() => Ticket, (ticket) => ticket.project)
  tickets: Ticket[];
}
