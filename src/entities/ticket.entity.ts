import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Project } from './project.entity';
import { Member } from './member.entity';

@Entity({
  name: 'tickets',
})
export class Ticket {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'member_id',
    type: 'integer',
  })
  memberId?: number;

  @Column({
    name: 'project_id',
    type: 'integer',
  })
  projectId?: number;

  @Column({
    type: 'varchar',
    length: 120,
    name: 'title',
  })
  title: string;

  @Column({
    name: 'content',
    type: 'varchar',
  })
  content: string;

  @Column({
    name: 'deadline',
    type: 'timestamp',
  })
  deadline: Date;

  @Column({
    name: 'assign',
    type: 'varchar',
    length: 255,
  })
  assign?: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updatedAt?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true,
  })
  deletedAt?: Date;

  @ManyToOne(() => Member, (member) => member.id)
  @JoinColumn({
    name: 'member_id',
  })
  member?: Member;

  @ManyToOne(() => Project, (project) => project.id)
  @JoinColumn({
    name: 'project_id',
  })
  project?: Project;
}
