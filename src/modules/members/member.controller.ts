import {
  Controller,
  Get,
  Param,
  Query,
  Post,
  Body,
  Patch,
} from '@nestjs/common';
import { MemberDTO } from 'src/dtos/members/member.dto';
import { Member } from 'src/entities/member.entity';
import { MemberService } from './member.service';

@Controller('members')
export class MemberController {
  constructor(private readonly memberService: MemberService) {}

  @Get(':id')
  async getHello(@Param('id') id: number): Promise<Member> {
    return this.memberService.takeById(id);
  }

  @Get()
  async getAll(
    @Query('name') name: string,
    @Query('username') username: string,
  ): Promise<Member[]> {
    return this.memberService.getAll(name, username);
  }

  @Post()
  async create(@Body() memberDto: MemberDTO) {
    await this.memberService.create(memberDto);
  }

  @Patch('/:id')
  async update(@Param() id: number, @Body() memberDto: MemberDTO) {
    await this.memberService.update(id, memberDto);
  }
}
