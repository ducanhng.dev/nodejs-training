import { Injectable, NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { MemberDTO } from 'src/dtos/members/member.dto';
import { Member } from 'src/entities/member.entity';
import { MemberRepository } from 'src/repositories/member.repository';

@Injectable()
export class MemberService {
  constructor(private readonly memberRepository: MemberRepository) {}

  async takeById(memberId: number): Promise<Member> {
    const member = await this.memberRepository.findOne(memberId);
    if (!member) {
      throw new NotFoundException();
    }
    return member;
  }

  async getAll(name: string, username: string): Promise<Member[]> {
    const result = await this.memberRepository.createQueryBuilder();

    if (name !== undefined) {
      result.where('name = :name', { name: name });
    }
    if (name !== undefined) {
      result.where('username = :username', { username: username });
    }

    return result.getMany();
  }

  async create(memberDto: MemberDTO) {
    const member = plainToClass(Member, memberDto);
    await this.memberRepository.save(member);
  }

  async update(id: number, memberDto: MemberDTO) {
    const member = plainToClass(Member, memberDto);
    await this.memberRepository.update(id, member);
  }
}
