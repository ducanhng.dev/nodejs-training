import { IsNotEmpty, IsString, Length } from 'class-validator';

export class MemberDTO {
  @IsString()
  @Length(1, 255)
  @IsNotEmpty()
  name: string;

  @IsString()
  @Length(1, 255)
  @IsNotEmpty()
  username: string;

  @IsString()
  @Length(8, 255)
  @IsNotEmpty()
  password: string;

  @IsString()
  @Length(1, 255)
  @IsNotEmpty()
  avatar?: string;
}
