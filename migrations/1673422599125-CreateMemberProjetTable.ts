import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateMemberProjetTable1673422599125
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'member_projects',
        columns: [
          {
            name: 'member_id',
            type: 'integer',
          },
          {
            name: 'project_id',
            type: 'integer',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'member_projects',
      new TableForeignKey({
        name: 'FK_MemberProjects_Members',
        columnNames: ['member_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'members',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),
    );

    await queryRunner.createForeignKey(
      'member_projects',
      new TableForeignKey({
        name: 'FK_MemberProjects_Projects',
        columnNames: ['project_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'projects',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'member_projects',
      'FK_MemberProjects_Members',
    );
    await queryRunner.dropForeignKey(
      'member_projects',
      'FK_MemberProjects_Projects',
    );
    await queryRunner.dropTable('member_projects');
  }
}
