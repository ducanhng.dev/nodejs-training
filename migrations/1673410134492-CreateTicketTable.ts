import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateTicketTable1673410134492 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'tickets',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'member_id',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'project_id',
            type: 'integer',
          },
          {
            name: 'title',
            type: 'varchar',
            length: '120',
          },
          {
            name: 'content',
            type: 'varchar',
          },
          {
            name: 'deadline',
            type: 'datetime',
          },
          {
            name: 'created_at',
            type: 'datetime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'datetime',
            isNullable: true,
          },
          {
            name: 'deleted_at',
            type: 'datetime',
            isNullable: true,
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'tickets',
      new TableForeignKey({
        name: 'Fk_Tickets_Members',
        columnNames: ['member_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'members',
      }),
    );

    await queryRunner.createForeignKey(
      'tickets',
      new TableForeignKey({
        name: 'Fk_Tickets_Projects',
        columnNames: ['project_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'projects',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('tickets', 'Fk_Tickets_Members');
    await queryRunner.dropForeignKey('tickets', 'Fk_Tickets_Projects');
    await queryRunner.dropTable('tickets');
  }
}
